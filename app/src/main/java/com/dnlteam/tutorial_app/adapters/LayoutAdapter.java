package com.dnlteam.tutorial_app.adapters;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.dnlteam.tutorial_app.R;

import java.util.List;

public class LayoutAdapter extends BaseRecyclerAdapter{

    TextView tvName;

    public LayoutAdapter(Context baseContext, List<String> data, ItemListener itemListener) {
        super(baseContext);
        baseData = data;
        baseItemListener = itemListener;
        layoutId = R.layout.layout_item;
    }


    @Override
    public View getView(View view) {
        tvName = bind(R.id.tvLayoutName);
        return view;
    }

    @Override
    public void onBindViewHolder(int position, Object value) {
        if(tvName.getTag() == null){
            tvName.setTag(position);
            String tutorialName = (String) value;
            tvName.setText(tutorialName);
        }
    }


}
