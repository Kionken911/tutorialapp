package com.dnlteam.tutorial_app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dnlteam.tutorial_app.R;

import java.util.List;

public class TutorialAdapter extends BaseRecyclerAdapter{

    TextView tvName;

    public TutorialAdapter(Context baseContext, List<String> data, ItemListener itemListener) {
        super(baseContext);
        baseData = data;
        baseItemListener = itemListener;
        layoutId = R.layout.tutorial_item;
    }


    @Override
    public View getView(View view) {
        tvName = bind(R.id.tvTutorialName);
        return view;
    }

    @Override
    public void onBindViewHolder(int position, Object value) {
        if(tvName.getTag() == null){
            tvName.setTag(position);
            String tutorialName = (String) value;
            tvName.setText(tutorialName);
            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    baseItemListener.onSelected(position);
                }
            });
        }
    }
}
