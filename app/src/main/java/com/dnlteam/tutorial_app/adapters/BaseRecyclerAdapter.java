package com.dnlteam.tutorial_app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseRecyclerAdapter extends RecyclerView.Adapter<BaseRecyclerAdapter.BaseViewHolder> {

    public int layoutId;
    protected List<?> baseData = new ArrayList<>();
    Context baseContext;
    ItemListener baseItemListener;
    public View item;

    public BaseRecyclerAdapter(Context baseContext) {
        this.baseContext = baseContext;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, final int position) {
        onBindViewHolder(position, baseData.get(position));
    }

    public abstract View getView(View view);

    @Override
    public int getItemCount() {
        return baseData == null ? 0 : baseData.size();
    }

    //override 2 methods(getItemId and getItemViewType) to fix bug load wrong value when fast scroll during
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public abstract void onBindViewHolder(int position, Object value);

    class BaseViewHolder extends RecyclerView.ViewHolder{
        public BaseViewHolder(@NonNull View view) {
            super(view);
            item = view;
            getView(item);
        }
    }

    public <T extends View> T bind(int id) {
        return item.findViewById(id);
    }

    public interface ItemListener{
        void onSelected(int position);
    }
}
