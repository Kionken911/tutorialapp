package com.dnlteam.tutorial_app.activities;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dnlteam.tutorial_app.R;
import com.dnlteam.tutorial_app.adapters.BaseRecyclerAdapter;
import com.dnlteam.tutorial_app.adapters.LayoutAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LayoutListActivity extends BaseActivity implements BaseRecyclerAdapter.ItemListener {
    RecyclerView layoutListRecyclerView;
    LayoutAdapter layoutAdapter;

    @Override
    int getLayoutId() {
        return R.layout.activity_layout_list;
    }

    @Override
    void onCreateActivity(Bundle savedInstanceState) {

    }

    @Override
    Toolbar getToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.layout_list_activity_title));
        return toolbar;
    }

    @Override
    void initComponent() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        layoutListRecyclerView = findViewById(R.id.recycleView);
        setupTutorialList();
    }

    @Override
    void setListener() {

    }

    @Override
    public void onClick(View v) {

    }

    private void setupTutorialList() {
        List<String> layoutList = new ArrayList<>();
        String [] layoutArr = getResources().getStringArray(R.array.layout_items);
        Collections.addAll(layoutList, layoutArr);
        layoutAdapter = new LayoutAdapter(this, layoutList, this::onSelected);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutListRecyclerView.setLayoutManager(linearLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(layoutListRecyclerView.getContext(), linearLayoutManager.getOrientation());
        GradientDrawable drawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[]{0xFFBB86FC, 0xFF03DAC5, 0xFFBB86FC});
        drawable.setSize(5,5);
        dividerItemDecoration.setDrawable(drawable);
        layoutListRecyclerView.addItemDecoration(dividerItemDecoration);
        layoutListRecyclerView.setAdapter(layoutAdapter);

    }

    @Override
    public void onSelected(int position) {

    }
}
