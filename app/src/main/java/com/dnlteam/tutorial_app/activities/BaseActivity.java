package com.dnlteam.tutorial_app.activities;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener {

    abstract int getLayoutId();
    abstract void onCreateActivity(Bundle savedInstanceState);
    abstract Toolbar getToolbar();
    abstract void initComponent();
    abstract void setListener();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        onCreateActivity(savedInstanceState);
        setSupportActionBar(getToolbar());
        initComponent();
        setListener();
    }


}
