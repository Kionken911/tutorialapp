package com.dnlteam.tutorial_app.activities;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;

import com.dnlteam.tutorial_app.R;
import com.dnlteam.tutorial_app.adapters.BaseRecyclerAdapter;
import com.dnlteam.tutorial_app.adapters.TutorialAdapter;
import com.dnlteam.tutorial_app.utils.Utils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class MainActivity extends BaseActivity implements BaseRecyclerAdapter.ItemListener {

    RecyclerView mainRecyclerView;
    TutorialAdapter tutorialAdapter;

    @Override
    int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    Toolbar getToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.main_activity_title));
        return toolbar;
    }

    @Override
    void initComponent() {
        mainRecyclerView = findViewById(R.id.recycleView);
        setupTutorialList();

    }

    @Override
    void setListener() {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    void onCreateActivity(Bundle savedInstanceState) {

    }

    private void setupTutorialList() {
        List<String> tutorialList = new ArrayList<>();
        String [] tutorialArr = getResources().getStringArray(R.array.tutorial_items);
        Collections.addAll(tutorialList, tutorialArr);
        tutorialAdapter = new TutorialAdapter(this, tutorialList, this::onSelected);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mainRecyclerView.setLayoutManager(linearLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mainRecyclerView.getContext(), linearLayoutManager.getOrientation());
        GradientDrawable drawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[]{0xFFBB86FC, 0xFF03DAC5, 0xFFBB86FC});
        drawable.setSize(5,5);
        dividerItemDecoration.setDrawable(drawable);
        mainRecyclerView.addItemDecoration(dividerItemDecoration);
        mainRecyclerView.setAdapter(tutorialAdapter);

    }


    @Override
    public void onSelected(int position) {
        switch (position){
            case 0:{
                Utils.navigateToNextScreen(this,LayoutListActivity.class,null,false);
                break;
            }
            default:
                break;
        }
    }
}