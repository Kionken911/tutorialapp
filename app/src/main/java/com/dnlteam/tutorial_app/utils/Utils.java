package com.dnlteam.tutorial_app.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Utils {
    public static void navigateToNextScreen(Activity currentActivity, Class nextScreen, Bundle bundle, boolean isFinish){
        Intent intent = new Intent(currentActivity, nextScreen);
        if(bundle!=null){
            intent.putExtras(bundle);
        }
        currentActivity.startActivity(intent);
        if(isFinish){
            currentActivity.finish();
        }
    }

    public static <T> T copy(T originalObject, Class<T> classInfo){
        Gson gson = new GsonBuilder().create();
        String data = gson.toJson(originalObject);
        T copyObject = gson.fromJson(data, classInfo);
        return copyObject;
    }
}
